const express = require('express');
const app = express();
var bodyParser = require('body-parser')
const morgan = require('morgan');

//settings
app.set('port', process.env.Port || 4002);
app.set('json spaces', 2);

//middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//routes
app.use(require('./routes/index'))

//start the server
app.listen(app.get('port'), () => {
  console.log(`Server is running on port ${app.get('port')}`);
});