const sql = require("mssql");
const config = require("./dbconfig");

const obtenerPool = () => {
  return new Promise((resolve, reject) => {
    sql.on("error", (err) =>
      err ? console.log("Error de connecion", err) : console.log("Todo bien BD")
    );
    sql.connect(config).then((pool) => {
        resolve(pool);
      }).catch((err) => reject(err));
  });
};

const cerrarPool = (pool) => {
  sql.close();
};
module.exports = { obtenerPool, cerrarPool };