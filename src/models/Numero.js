var connecion = require('../database/connection');
const sql = require('mssql');

const getNumeros = () => {
  return new Promise((resolve, reject) => {
    connecion.obtenerPool().then((pool) => {
      return pool.request().query("SELECT * FROM [BDCeCo].[dbo].[Numeros]");
    }).then((result) => {
      resolve(result);
    }).catch((error) => {
      reject(error);
    });
  });
}

const insertNumeros = (IdNumero, Numero, NombreNumero, UltimaLlamada, PermitirLlamada) =>{
  return new Promise((resolve, reject) => {
    let insert = "INSERT INTO [BDCeCo].[dbo].[BitacoraNumeros] ([IdNumero],[Numero],[NombreNumero],[UltimaLlamada],[VecesLlamada],[PermitirLlamada],[Detalles]) values("+
    "@IdNumero," +
    "@Numero," +
    "@NombreNumero," +
    "@UltimaLlamada," +
    "@VecesLlamada," +
    "@PermitirLlamada," +
    "null" ;
    connecion.obtenerPool().then((pool) => { 
      const transaccion = new sql.Transaction(pool);
      transaccion.begin((err) => {
        let rolledBack = false;
        transaccion.on("rollback",(aborted)=>{
          rolledBack = true;
        });
        new sql.Request(transaccion)
        .input("IdNumero", sql.Int, IdNumero)
        .input("Numero",sql.VarChar, Numero)
        .input("NombreNumero",sql.VarChar, NombreNumero)
        .input("UltimaLlamada",sql.Int, UltimaLlamada)
        .input("VecesLlamada",sql.Int, VecesLlamada)
        .input("PermitirLlamada",sql.Int, PermitirLlamada)
        .query(insert, (err, result) =>{
          if(err){
            if (!rolledBack){
              transaccion.rollback((err) =>{});
              reject(err);
            }
          } else {
            transaccion.commit((err) => {
              if (err) {} else {
                resolve("Insert Correcto");
              }
            });
          }
        });
      });
    });
  });
}
// IdBitacora	int	Unchecked
// IdNumero	int	Unchecked
// Numero	varchar(50)	Checked
// NombreNumero	varchar(50)	Checked
// UltimaLlamada	int	Checked
// VecesLlamada	int	Checked
// PermitirLlamada	int	Checked
// Detalles	varchar(1200)	Checked
// 		Unchecked
// async function getNumeros() {
//   try {
//     let pool = await sql.connect(config);
//     let numeros = await pool.request().query("SELECT * FROM [BDCeCo].[dbo].[Numeros]");
//     return numeros.recordsets;
//   } catch (error) {
//     console.log('Error2', error)
//   }
// }

module.exports = {
  getNumeros,
  insertNumeros
}