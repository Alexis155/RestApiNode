var connecion = require('../database/connection');
const sql = require('mssql');

const getCategorias = () =>{
    return new Promise((resolve, reject) => {
        connecion.obtenerPool().then((pool) => {
            return pool.request().query("SELECT * FROM [BDCeCo].[dbo].[CATTIPOEXTORCION]");
        
        }).then((result) => {
            resolve(result);
        }).catch((error) => {
            reject(error);
        });
    });
}

module.exports = {
    getCategorias
  }