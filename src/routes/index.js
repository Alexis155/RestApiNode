const { Router } = require('express');
const router = Router();
const DBnumeros = require('../models/Numero');
const DBcategorias = require('../models/Categoria');

router.get('/', (req, res) => {
  console.log("Hola");
})
router.post('/numeros', async (req, res) => {
  try {
    let type = req.body.numeros;
    let numeros = await DBnumeros.getNumeros();    
    res.json({numeros: numeros.recordset});
  } catch (error) {
    res.status(500).json({ error: error });
  }

});

router.post('/numero', async (req, res) => {
  try {
    let type = req.body.numeros;
    let numeros = await DBnumeros.getNumeros();    
    res.json(numeros.recordset);
  } catch (error) {
    res.status(500).json({ error: error });
  }

});

router.post('/categorias', async (req, res) => {
  try {
    let categorias = await DBcategorias.getCategorias();
    res.json({ "categorias": categorias.recordsets });
  } catch (error) {
    res.status(500).json({ error: error });
  }

});

router.post('/insert',async (req, res) => {

  try {
    let type = req.body.numeros;
    console.log("insertNumeros",type);
    let numeros = await DBnumeros.insertNumeros();
    res.json({ "numeros": numeros.recordset });
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

module.exports = router;